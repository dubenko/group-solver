type Cell = number;
const CellUtils = {
  color: (cell: Cell) => ["red", "green", "blue", "orange"][cell],
}

export { Cell, CellUtils };